from flask import Flask
import locale
from flask_login import LoginManager

app = Flask(__name__)
app.config.from_object('config')
app.config['TESTING'] = False
login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = "login"



from app import views