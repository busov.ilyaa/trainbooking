from app import app
import json
import flask
from flask import render_template, jsonify, request, abort, url_for, redirect
from forms import LoginForm, RegistrationForm
from flask_login import current_user, login_user,logout_user, login_required
from app import login_manager
from urllib.parse import urlparse, urljoin
import requests

from User import User

@app.route('/')
def index():
    return render_template('index.html', user=current_user)

@login_required
@app.route('/wagons/<int:id_trip>/<string:from_station>/<string:to_station>', methods=['GET', 'POST'])
def wagons(id_trip, from_station, to_station):
    r = requests.get('http://127.0.0.1:5051/trains/wagons/{}/{}/{}'.format(id_trip, from_station, to_station))
    data = r.json()
    return render_template('wagons.html', wagons=data, user=current_user, from_station=from_station, to_station=to_station)

@app.route('/places/<int:id_wagon>/<string:from_station>/<string:to_station>', methods=['GET','POST'])
@login_required
def get_free_places(id_wagon, from_station, to_station):
    if request.method=='GET':
        r = requests.get('http://127.0.0.1:5051/places/{}/{}/{}'.format(id_wagon, from_station, to_station))
        data = r.json()
        return render_template('places.html', places=data, user=current_user, id_wagon=id_wagon, from_station=from_station, to_station=to_station)
    elif request.method=='POST':
        id_place = request.json['id_place']
        requests.post('http://127.0.0.1:5051/ticket/buy', json={'from_station':from_station,
         'to_station':to_station, 'id_passenger':current_user.id, 'id_place':id_place})
        print(url_for('index'))
        return redirect(url_for('index'))


@login_manager.user_loader
def load_user(username):
    r = requests.get('http://127.0.0.1:5051/user/{}'.format(username))
    json_data = json.loads(r.text)
    if len(json_data) == 0:
        return None
    user = User(json_data['id_passenger'], json_data['username'], json_data['name'], json_data['surename'], None)
    user.password = json_data['password']
    return user


def is_safe_url(target):
    ref_url = urlparse(request.host_url)
    test_url = urlparse(urljoin(request.host_url, target))
    return test_url.scheme in ('http', 'https') and \
           ref_url.netloc == test_url.netloc


@app.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = LoginForm()
    if request.method == 'POST':
        username = request.form['username']
        user = load_user(username)
        if user is None or not user.check_password(form.password.data):
            return redirect(url_for('login'))
        login_user(user)
        return redirect(url_for('index'))
    return render_template('login.html', form=form)


@app.route('/logout', methods=['GET'])
def logout():
    logout_user()
    return redirect(url_for('index'))

@app.route('/register', methods=['GET', 'POST'])
def register():
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = RegistrationForm()
    if form.validate_on_submit():
        username = request.form['username']
        name = request.form['name']
        surename = request.form['surename']
        password = request.form['password']

        user = User(0, username, name, surename, password)
        requests.post('http://127.0.0.1:5051/user/add', json=user.__dict__)
        return redirect(url_for('login'))
    return render_template('register.html', form=form)


@app.route('/user/tickets', methods=['GET'])
@login_required
def user_page():
    r = requests.get('http://127.0.0.1:5051/user/tickets/{}'.format(current_user.id))
    data = r.json()

    return render_template('user.html', tickets=data, user=current_user)