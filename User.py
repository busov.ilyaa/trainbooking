from flask_login import UserMixin
import requests
import hashlib


class User(UserMixin):
    def __init__(self, id, username, name, surename, password):
        self.id = id
        self.username = username
        self.name = name
        self.surename = surename
        self.password = self.set_password(password)

    def get_id(self):
        return '{}'.format(self.username)

    def check_password(self, password):
        return encode(password) == self.password

    def set_password(self, password):
        return encode(password)
    

def encode(data):
    if data is None:
        return None
    return hashlib.sha256(data.encode()).hexdigest()